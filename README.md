# Aula Ettore

## Sobre o projeto

Uma replica do jogo 'Termo'.

Projeto realizado pelo Grupo 3 da máteria 'Engenharia de software' com o professor Ettore como orientador.

## Informativos
        [FrontEnd]
    HTML
    CSS
---
        [BackEnd]
    JavaScript
    Python
---
        [Dependencias]
    Package    |  Version
    ----------- ----------

    asgiref      3.7.2
    certifi      2023.11.17
    distlib      0.3.8
    Django       5.0
    filelock     3.13.1
    pip          24.0
    platformdirs 4.1.0
    psycopg2     2.9.9
    setuptools   69.0.3
    sqlparse     0.4.4
    tzdata       2023.4
    virtualenv   20.25.0

## Observação
### Utilizamos outra plataforma para usufruirmos das ferramentas git, sendo assim, foi preciso migrar as mudanças da outra plataforma para o GitLab, por esse motivo os commits estão mais 'grosseiros' nesse projeto, porém todos os integrantes colaboraram com desenvolvimento do projeto.